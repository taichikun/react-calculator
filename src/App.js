import { useState } from 'react'
import 'bootstrap/dist/css/bootstrap.min.css'
import './App.css';
import { Button, Form, Container, Row, Col } from 'react-bootstrap'

export default function App() {

  const [number1, setNumber1] = useState("");
  const [number2, setNumber2] = useState("");
  const [result, setResult] = useState(0);

  const add = () => {
    setResult(eval(number1) + eval(number2));
  }

  const subtract = () => {
    setResult(number1 - number2);
  }

  const mult = () => {
    setResult(number1 * number2);
  }

  const div = () => {
    setResult(number1 / number2);
  }

  const clear = () => {
    setResult(0);
    setNumber1("");
    setNumber2("");
  }


  return (
    <Container>
    <h1 className='text-center mt-5'>Calculator</h1>
    <h2 className='text-center mt-5' type="number">{result}</h2>
      <Form className='mt-5'>
        <Form.Group>
            <Row>
              <Col>
                <Form.Control type="number" value={number1} onChange={e => setNumber1(e.target.value)}/>
              </Col>
              <Col>
                <Form.Control type="number" value={number2} onChange={e => setNumber2(e.target.value)}/>
              </Col>
            </Row>
        </Form.Group>
        <div className='mt-5 text-center'>
          <Button className='mx-3' variant="primary" onClick={e => add()}>Add</Button>
          <Button className='mx-3' variant="primary" onClick={e => subtract()}>Subtract</Button>
          <Button className='mx-3' variant="primary" onClick={e => mult()}>Multiply</Button>
          <Button className='mx-3' variant="primary" onClick={e => div()}>Divide</Button>
          <Button className='mx-3' variant="primary" onClick={e => clear()}>Reset</Button>
        </div>
      </Form>
    </Container>
  );
}
